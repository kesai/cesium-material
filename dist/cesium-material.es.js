import * as Cesium from "cesium";
var cellular_default = `vec3 _czm_permute289(vec3 x)\r
{\r
    return mod((34.0 * x + 1.0) * x, 289.0);\r
}

/**\r
 * DOC_TBA\r
 *\r
 * Implemented by Stefan Gustavson, and distributed under the MIT License.  {@link http:
 *\r
 * @name czm_cellular\r
 * @glslFunction\r
 *\r
 * @see Stefan Gustavson's chapter, <i>Procedural Textures in GLSL</i>, in <a href="http:
 */\r
vec2 czm_cellular(vec2 P)

{\r
#define K 0.142857142857 
#define Ko 0.428571428571 
#define jitter 1.0 
    vec2 Pi = mod(floor(P), 289.0);\r
    vec2 Pf = fract(P);\r
    vec3 oi = vec3(-1.0, 0.0, 1.0);\r
    vec3 of = vec3(-0.5, 0.5, 1.5);\r
    vec3 px = _czm_permute289(Pi.x + oi);\r
    vec3 p = _czm_permute289(px.x + Pi.y + oi); 
    vec3 ox = fract(p*K) - Ko;\r
    vec3 oy = mod(floor(p*K),7.0)*K - Ko;\r
    vec3 dx = Pf.x + 0.5 + jitter*ox;\r
    vec3 dy = Pf.y - of + jitter*oy;\r
    vec3 d1 = dx * dx + dy * dy; 
    p = _czm_permute289(px.y + Pi.y + oi); 
    ox = fract(p*K) - Ko;\r
    oy = mod(floor(p*K),7.0)*K - Ko;\r
    dx = Pf.x - 0.5 + jitter*ox;\r
    dy = Pf.y - of + jitter*oy;\r
    vec3 d2 = dx * dx + dy * dy; 
    p = _czm_permute289(px.z + Pi.y + oi); 
    ox = fract(p*K) - Ko;\r
    oy = mod(floor(p*K),7.0)*K - Ko;\r
    dx = Pf.x - 1.5 + jitter*ox;\r
    dy = Pf.y - of + jitter*oy;\r
    vec3 d3 = dx * dx + dy * dy; 
    
    vec3 d1a = min(d1, d2);\r
    d2 = max(d1, d2); 
    d2 = min(d2, d3); 
    d1 = min(d1a, d2); 
    d2 = max(d1a, d2); 
    d1.xy = (d1.x < d1.y) ? d1.xy : d1.yx; 
    d1.xz = (d1.x < d1.z) ? d1.xz : d1.zx; 
    d1.yz = min(d1.yz, d2.yz); 
    d1.y = min(d1.y, d1.z); 
    d1.y = min(d1.y, d2.x); 
    return sqrt(d1.xy);\r
}`;
var snoise_default = `vec4 _czm_mod289(vec4 x)\r
{\r
  return x - floor(x * (1.0 / 289.0)) * 289.0;\r
}

vec3 _czm_mod289(vec3 x)\r
{\r
    return x - floor(x * (1.0 / 289.0)) * 289.0;\r
}

vec2 _czm_mod289(vec2 x)\r
{\r
    return x - floor(x * (1.0 / 289.0)) * 289.0;\r
}

float _czm_mod289(float x)\r
{\r
    return x - floor(x * (1.0 / 289.0)) * 289.0;\r
}

vec4 _czm_permute(vec4 x)\r
{\r
    return _czm_mod289(((x*34.0)+1.0)*x);\r
}

vec3 _czm_permute(vec3 x)\r
{\r
    return _czm_mod289(((x*34.0)+1.0)*x);\r
}

float _czm_permute(float x)\r
{\r
    return _czm_mod289(((x*34.0)+1.0)*x);\r
}

vec4 _czm_taylorInvSqrt(vec4 r)\r
{\r
    return 1.79284291400159 - 0.85373472095314 * r;\r
}

float _czm_taylorInvSqrt(float r)\r
{\r
    return 1.79284291400159 - 0.85373472095314 * r;\r
}

vec4 _czm_grad4(float j, vec4 ip)\r
{\r
    const vec4 ones = vec4(1.0, 1.0, 1.0, -1.0);\r
    vec4 p,s;

    p.xyz = floor( fract (vec3(j) * ip.xyz) * 7.0) * ip.z - 1.0;\r
    p.w = 1.5 - dot(abs(p.xyz), ones.xyz);\r
    s = vec4(lessThan(p, vec4(0.0)));\r
    p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www;

    return p;\r
}

/**\r
 * DOC_TBA\r
 *\r
 * Implemented by Ian McEwan, Ashima Arts, and distributed under the MIT License.  {@link https:
 *\r
 * @name czm_snoise\r
 * @glslFunction\r
 *\r
 * @see <a href="https:
 * @see Stefan Gustavson's paper <a href="http:
 */\r
float czm_snoise(vec2 v)\r
{\r
    const vec4 C = vec4(0.211324865405187,  
                        0.366025403784439,  
                       -0.577350269189626,  
                        0.024390243902439); 
    
    vec2 i  = floor(v + dot(v, C.yy) );\r
    vec2 x0 = v -   i + dot(i, C.xx);

    
    vec2 i1;\r
    
    
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\r
    
    
    
    vec4 x12 = x0.xyxy + C.xxzz;\r
    x12.xy -= i1;

    
    i = _czm_mod289(i); 
    vec3 p = _czm_permute( _czm_permute( i.y + vec3(0.0, i1.y, 1.0 )) + i.x + vec3(0.0, i1.x, 1.0 ));

    vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);\r
    m = m*m ;\r
    m = m*m ;

    
    
    vec3 x = 2.0 * fract(p * C.www) - 1.0;\r
    vec3 h = abs(x) - 0.5;\r
    vec3 ox = floor(x + 0.5);\r
    vec3 a0 = x - ox;

    
    
    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

    
    vec3 g;\r
    g.x  = a0.x  * x0.x  + h.x  * x0.y;\r
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;\r
    return 130.0 * dot(m, g);\r
}

float czm_snoise(vec3 v)\r
{\r
    const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;\r
    const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

    
    vec3 i  = floor(v + dot(v, C.yyy) );\r
    vec3 x0 =   v - i + dot(i, C.xxx) ;

    
    vec3 g = step(x0.yzx, x0.xyz);\r
    vec3 l = 1.0 - g;\r
    vec3 i1 = min( g.xyz, l.zxy );\r
    vec3 i2 = max( g.xyz, l.zxy );

    
    
    
    
    vec3 x1 = x0 - i1 + C.xxx;\r
    vec3 x2 = x0 - i2 + C.yyy; 
    vec3 x3 = x0 - D.yyy;      

    
    i = _czm_mod289(i);\r
    vec4 p = _czm_permute( _czm_permute( _czm_permute(\r
                i.z + vec4(0.0, i1.z, i2.z, 1.0 ))\r
              + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))\r
              + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

    
    
    float n_ = 0.142857142857; 
    vec3  ns = n_ * D.wyz - D.xzx;

    vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  

    vec4 x_ = floor(j * ns.z);\r
    vec4 y_ = floor(j - 7.0 * x_ );    

    vec4 x = x_ *ns.x + ns.yyyy;\r
    vec4 y = y_ *ns.x + ns.yyyy;\r
    vec4 h = 1.0 - abs(x) - abs(y);

    vec4 b0 = vec4( x.xy, y.xy );\r
    vec4 b1 = vec4( x.zw, y.zw );

    
    
    vec4 s0 = floor(b0)*2.0 + 1.0;\r
    vec4 s1 = floor(b1)*2.0 + 1.0;\r
    vec4 sh = -step(h, vec4(0.0));

    vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;\r
    vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

    vec3 p0 = vec3(a0.xy,h.x);\r
    vec3 p1 = vec3(a0.zw,h.y);\r
    vec3 p2 = vec3(a1.xy,h.z);\r
    vec3 p3 = vec3(a1.zw,h.w);

    
    vec4 norm = _czm_taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\r
    p0 *= norm.x;\r
    p1 *= norm.y;\r
    p2 *= norm.z;\r
    p3 *= norm.w;

    
    vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);\r
    m = m * m;\r
    return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),\r
                                dot(p2,x2), dot(p3,x3) ) );\r
}

float czm_snoise(vec4 v)\r
{\r
    const vec4  C = vec4( 0.138196601125011,  
                          0.276393202250021,  
                          0.414589803375032,  
                         -0.447213595499958); 

    
    #define F4 0.309016994374947451

    
    vec4 i  = floor(v + dot(v, vec4(F4)) );\r
    vec4 x0 = v -   i + dot(i, C.xxxx);

    

    
    vec4 i0;\r
    vec3 isX = step( x0.yzw, x0.xxx );\r
    vec3 isYZ = step( x0.zww, x0.yyz );\r
    
    i0.x = isX.x + isX.y + isX.z;\r
    i0.yzw = 1.0 - isX;\r
    
    i0.y += isYZ.x + isYZ.y;\r
    i0.zw += 1.0 - isYZ.xy;\r
    i0.z += isYZ.z;\r
    i0.w += 1.0 - isYZ.z;

    
    vec4 i3 = clamp( i0, 0.0, 1.0 );\r
    vec4 i2 = clamp( i0-1.0, 0.0, 1.0 );\r
    vec4 i1 = clamp( i0-2.0, 0.0, 1.0 );

    
    
    
    
    
    vec4 x1 = x0 - i1 + C.xxxx;\r
    vec4 x2 = x0 - i2 + C.yyyy;\r
    vec4 x3 = x0 - i3 + C.zzzz;\r
    vec4 x4 = x0 + C.wwww;

    
    i = _czm_mod289(i);\r
    float j0 = _czm_permute( _czm_permute( _czm_permute( _czm_permute(i.w) + i.z) + i.y) + i.x);\r
    vec4 j1 = _czm_permute( _czm_permute( _czm_permute( _czm_permute (\r
               i.w + vec4(i1.w, i2.w, i3.w, 1.0 ))\r
             + i.z + vec4(i1.z, i2.z, i3.z, 1.0 ))\r
             + i.y + vec4(i1.y, i2.y, i3.y, 1.0 ))\r
             + i.x + vec4(i1.x, i2.x, i3.x, 1.0 ));

    
    
    vec4 ip = vec4(1.0/294.0, 1.0/49.0, 1.0/7.0, 0.0) ;

    vec4 p0 = _czm_grad4(j0,   ip);\r
    vec4 p1 = _czm_grad4(j1.x, ip);\r
    vec4 p2 = _czm_grad4(j1.y, ip);\r
    vec4 p3 = _czm_grad4(j1.z, ip);\r
    vec4 p4 = _czm_grad4(j1.w, ip);

    
    vec4 norm = _czm_taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));\r
    p0 *= norm.x;\r
    p1 *= norm.y;\r
    p2 *= norm.z;\r
    p3 *= norm.w;\r
    p4 *= _czm_taylorInvSqrt(dot(p4,p4));

    
    vec3 m0 = max(0.6 - vec3(dot(x0,x0), dot(x1,x1), dot(x2,x2)), 0.0);\r
    vec2 m1 = max(0.6 - vec2(dot(x3,x3), dot(x4,x4)            ), 0.0);\r
    m0 = m0 * m0;\r
    m1 = m1 * m1;\r
    return 49.0 * ( dot(m0*m0, vec3( dot( p0, x0 ), dot( p1, x1 ), dot( p2, x2 )))\r
                  + dot(m1*m1, vec2( dot( p3, x3 ), dot( p4, x4 ) ) ) ) ;\r
}`;
var AsphaltMaterial_default = "uniform vec4 asphaltColor;\r\nuniform float bumpSize;\r\nuniform float roughness;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  \n  \n  vec4 color = asphaltColor;\r\n  vec2 st = materialInput.st;\r\n  vec2 F = czm_cellular(st / bumpSize);\r\n  color.rgb -= (F.x / F.y) * 0.1;\n\n  \n  float noise = czm_snoise(st / bumpSize);\r\n  noise = pow(noise, 5.0) * roughness;\r\n  color.rgb += noise;\n\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var BlobMaterial_default = "uniform vec4 lightColor;\r\nuniform vec4 darkColor;\r\nuniform float frequency;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  \n  vec2 F = czm_cellular(materialInput.st * frequency);\r\n  float t = 1.0 - F.x * F.x;\n\n  vec4 color = mix(lightColor, darkColor, t);\r\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var CementMaterial_default = "uniform vec4 cementColor;\r\nuniform float grainScale;\r\nuniform float roughness;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  float noise = czm_snoise(materialInput.st / grainScale);\r\n  noise = pow(noise, 5.0) * roughness;\n\n  vec4 color = cementColor;\r\n  color.rgb += noise;\n\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var ErosionMaterial_default = "uniform vec4 color;\r\nuniform float time;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  float alpha = 1.0;\r\n  if (time != 1.0)\r\n  {\r\n      float t = 0.5 + (0.5 * czm_snoise(materialInput.str / (1.0 / 10.0)));   \n\n      if (t > time)\r\n      {\r\n          alpha = 0.0;\r\n      }\r\n  }\n\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a * alpha;\n\n  return material;\r\n}";
var FacetMaterial_default = "uniform vec4 lightColor;\r\nuniform vec4 darkColor;\r\nuniform float frequency;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  \n  vec2 F = czm_cellular(materialInput.st * frequency);\r\n  float t = 0.1 + (F.y - F.x);\n\n  vec4 color = mix(lightColor, darkColor, t);\r\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var FresnelMaterial_default = "czm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  vec3 normalWC = normalize(czm_inverseViewRotation * material.normal);\r\n  vec3 positionWC = normalize(czm_inverseViewRotation * materialInput.positionToEyeEC);\r\n  float cosAngIncidence = max(dot(normalWC, positionWC), 0.0);\n\n  material.diffuse = mix(reflection.diffuse, refraction.diffuse, cosAngIncidence);\n\n  return material;\r\n}";
var GrassMaterial_default = "uniform vec4 grassColor;\r\nuniform vec4 dirtColor;\r\nuniform float patchiness;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  vec2 st = materialInput.st;\r\n  float noise1 = (czm_snoise(st * patchiness * 1.0)) * 1.0;\r\n  float noise2 = (czm_snoise(st * patchiness * 2.0)) * 0.5;\r\n  float noise3 = (czm_snoise(st * patchiness * 4.0)) * 0.25;\r\n  float noise = sin(noise1 + noise2 + noise3) * 0.1;\n\n  vec4 color = mix(grassColor, dirtColor, noise);\n\n  \n  float verticalNoise = czm_snoise(vec2(st.x * 100.0, st.y * 20.0)) * 0.02;\r\n  float horizontalNoise = czm_snoise(vec2(st.x * 20.0, st.y * 100.0)) * 0.02;\r\n  float stripeNoise = min(verticalNoise, horizontalNoise);\n\n  color.rgb += stripeNoise;\n\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var ReflectionMaterial_default = "uniform samplerCube cubeMap;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  vec3 normalWC = normalize(czm_inverseViewRotation * material.normal);\r\n  vec3 positionWC = normalize(czm_inverseViewRotation * materialInput.positionToEyeEC);\r\n  vec3 reflectedWC = reflect(positionWC, normalWC);\r\n  material.diffuse = textureCube(cubeMap, reflectedWC).channels;\n\n  return material;\r\n}";
var RefractionMaterial_default = "uniform samplerCube cubeMap;\r\nuniform float indexOfRefractionRatio;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  vec3 normalWC = normalize(czm_inverseViewRotation * material.normal);\r\n  vec3 positionWC = normalize(czm_inverseViewRotation * materialInput.positionToEyeEC);\r\n  vec3 refractedWC = refract(positionWC, -normalWC, indexOfRefractionRatio);\r\n  material.diffuse = textureCube(cubeMap, refractedWC).channels;\n\n  return material;\r\n}";
var TieDyeMaterial_default = "uniform vec4 lightColor;\r\nuniform vec4 darkColor;\r\nuniform float frequency;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  vec3 scaled = materialInput.str * frequency;\r\n  float t = abs(czm_snoise(scaled));\n\n  vec4 color = mix(lightColor, darkColor, t);\r\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
var WoodMaterial_default = "uniform vec4 lightWoodColor;\r\nuniform vec4 darkWoodColor;\r\nuniform float ringFrequency;\r\nuniform vec2 noiseScale;\r\nuniform float grainFrequency;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\n\n  \n  vec2 st = materialInput.st;\n\n  vec2 noisevec;\r\n  noisevec.x = czm_snoise(st * noiseScale.x);\r\n  noisevec.y = czm_snoise(st * noiseScale.y);\n\n  vec2 location = st + noisevec;\r\n  float dist = sqrt(location.x * location.x + location.y * location.y);\r\n  dist *= ringFrequency;\n\n  float r = fract(dist + noisevec[0] + noisevec[1]) * 2.0;\r\n  if(r > 1.0)\r\n      r = 2.0 - r;\n\n  vec4 color = mix(lightWoodColor, darkWoodColor, r);\n\n  \n  r = abs(czm_snoise(vec2(st.x * grainFrequency, st.y * grainFrequency * 0.02))) * 0.2;\r\n  color.rgb += lightWoodColor.rgb * r;\n\n  material.diffuse = color.rgb;\r\n  material.alpha = color.a;\n\n  return material;\r\n}";
Cesium.ShaderSource._czmBuiltinsAndUniforms.czm_cellular = cellular_default;
Cesium.ShaderSource._czmBuiltinsAndUniforms.czm_snoise = snoise_default;
Cesium.Material.AsphaltType = "Asphalt";
Cesium.Material._materialCache.addMaterial(Cesium.Material.AsphaltType, {
  fabric: {
    type: Cesium.Material.AsphaltType,
    uniforms: {
      asphaltColor: new Cesium.Color(0.15, 0.15, 0.15, 1),
      bumpSize: 0.02,
      roughness: 0.2
    },
    source: AsphaltMaterial_default
  },
  translucent: function(material) {
    return material.uniforms.asphaltColor.alpha < 1;
  }
});
Cesium.Material.BlobType = "Blob";
Cesium.Material._materialCache.addMaterial(Cesium.Material.BlobType, {
  fabric: {
    type: Cesium.Material.BlobType,
    uniforms: {
      lightColor: new Cesium.Color(1, 1, 1, 0.5),
      darkColor: new Cesium.Color(0, 0, 1, 0.5),
      frequency: 10
    },
    source: BlobMaterial_default
  },
  translucent: function(material) {
    var uniforms = material.uniforms;
    return uniforms.lightColor.alpha < 1 || uniforms.darkColor.alpha < 0;
  }
});
Cesium.Material.BrickType = "Brick";
Cesium.Material._materialCache.addMaterial(Cesium.Material.BrickType, {
  fabric: {
    type: Cesium.Material.BrickType,
    uniforms: {
      brickColor: new Cesium.Color(0.6, 0.3, 0.1, 1),
      mortarColor: new Cesium.Color(0.8, 0.8, 0.7, 1),
      brickSize: new Cesium.Cartesian2(0.3, 0.15),
      brickPct: new Cesium.Cartesian2(0.9, 0.85),
      brickRoughness: 0.2,
      mortarRoughness: 0.1
    },
    source: BlobMaterial_default
  },
  translucent: function(material) {
    var uniforms = material.uniforms;
    return uniforms.brickColor.alpha < 1 || uniforms.mortarColor.alpha < 1;
  }
});
Cesium.Material.CementType = "Cement";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CementType, {
  fabric: {
    type: Cesium.Material.CementType,
    uniforms: {
      cementColor: new Cesium.Color(0.95, 0.95, 0.85, 1),
      grainScale: 0.01,
      roughness: 0.3
    },
    source: CementMaterial_default
  },
  translucent: function(material) {
    return material.uniforms.cementColor.alpha < 1;
  }
});
Cesium.Material.ErosionType = "Erosion";
Cesium.Material._materialCache.addMaterial(Cesium.Material.ErosionType, {
  fabric: {
    type: Cesium.Material.ErosionType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.5),
      time: 1
    },
    source: ErosionMaterial_default
  },
  translucent: function(material) {
    return material.uniforms.color.alpha < 1;
  }
});
Cesium.Material.FacetType = "Facet";
Cesium.Material._materialCache.addMaterial(Cesium.Material.FacetType, {
  fabric: {
    type: Cesium.Material.FacetType,
    uniforms: {
      lightColor: new Cesium.Color(0.25, 0.25, 0.25, 0.75),
      darkColor: new Cesium.Color(0.75, 0.75, 0.75, 0.75),
      frequency: 10
    },
    source: FacetMaterial_default
  },
  translucent: function(material) {
    var uniforms = material.uniforms;
    return uniforms.lightColor.alpha < 1 || uniforms.darkColor.alpha < 0;
  }
});
Cesium.Material.FresnelType = "Fresnel";
Cesium.Material._materialCache.addMaterial(Cesium.Material.FresnelType, {
  fabric: {
    type: Cesium.Material.FresnelType,
    materials: {
      reflection: {
        type: Cesium.Material.ReflectionType
      },
      refraction: {
        type: Cesium.Material.RefractionType
      }
    },
    source: FresnelMaterial_default
  },
  translucent: false
});
Cesium.Material.GrassType = "Grass";
Cesium.Material._materialCache.addMaterial(Cesium.Material.GrassType, {
  fabric: {
    type: Cesium.Material.GrassType,
    uniforms: {
      grassColor: new Cesium.Color(0.25, 0.4, 0.1, 1),
      dirtColor: new Cesium.Color(0.1, 0.1, 0.1, 1),
      patchiness: 1.5
    },
    source: GrassMaterial_default
  },
  translucent: function(material) {
    var uniforms = material.uniforms;
    return uniforms.grassColor.alpha < 1 || uniforms.dirtColor.alpha < 1;
  }
});
Cesium.Material.ReflectionType = "Reflection";
Cesium.Material._materialCache.addMaterial(Cesium.Material.ReflectionType, {
  fabric: {
    type: Cesium.Material.ReflectionType,
    uniforms: {
      cubeMap: Cesium.Material.DefaultCubeMapId,
      channels: "rgb"
    },
    source: ReflectionMaterial_default
  },
  translucent: false
});
Cesium.Material.RefractionType = "Refraction";
Cesium.Material._materialCache.addMaterial(Cesium.Material.RefractionType, {
  fabric: {
    type: Cesium.Material.RefractionType,
    uniforms: {
      cubeMap: Cesium.Material.DefaultCubeMapId,
      channels: "rgb",
      indexOfRefractionRatio: 0.9
    },
    source: RefractionMaterial_default
  },
  translucent: false
});
Cesium.Material.TyeDyeType = "TieDye";
Cesium.Material._materialCache.addMaterial(Cesium.Material.TyeDyeType, {
  fabric: {
    type: Cesium.Material.TyeDyeType,
    uniforms: {
      lightColor: new Cesium.Color(1, 1, 0, 0.75),
      darkColor: new Cesium.Color(1, 0, 0, 0.75),
      frequency: 5
    },
    source: TieDyeMaterial_default
  },
  translucent: function(material) {
    var uniforms = material.uniforms;
    return uniforms.lightColor.alpha < 1 || uniforms.darkColor.alpha < 0;
  }
});
Cesium.Material.WoodType = "Wood";
Cesium.Material._materialCache.addMaterial(Cesium.Material.WoodType, {
  fabric: {
    type: Cesium.Material.WoodType,
    uniforms: {
      lightWoodColor: new Cesium.Color(0.6, 0.3, 0.1, 1),
      darkWoodColor: new Cesium.Color(0.4, 0.2, 0.07, 1),
      ringFrequency: 3,
      noiseScale: new Cesium.Cartesian2(0.7, 0.5),
      grainFrequency: 27
    },
    source: WoodMaterial_default
  },
  translucent: function(material) {
    let uniforms = material.uniforms;
    return uniforms.lightWoodColor.alpha < 1 || uniforms.darkWoodColor.alpha < 1;
  }
});
var CircleBlurMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st ;\r\n  vec2 center = vec2(0.5);\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  float r = 0.5 + sin(time) / 3.0;\r\n  float dis = distance(st, center);\r\n  float a = 0.0;\r\n  if(dis < r) {\r\n    a = 1.0 - smoothstep(0.0, r, dis);\r\n  }\r\n  material.alpha = pow(a,10.0) ;\r\n  material.diffuse = color.rgb * a * 3.0;\r\n  return material;\r\n}";
var CircleDiffuseMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nvec3 circlePing(float r, float innerTail,  float frontierBorder, float timeResetSeconds,  float radarPingSpeed,  float fadeDistance){\r\n  float t = fract(czm_frameNumber * speed / 1000.0);\r\n  float time = mod(t, timeResetSeconds) * radarPingSpeed;\r\n  float circle;\r\n  circle += smoothstep(time - innerTail, time, r) * smoothstep(time + frontierBorder,time, r);\r\n  circle *= smoothstep(fadeDistance, 0.0, r);\r\n  return vec3(circle);\r\n}\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st * 2.0  - 1.0 ;\r\n  vec2 center = vec2(0.);\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  vec3 flagColor;\r\n  float r = length(st - center) / 4.;\r\n  flagColor += circlePing(r, 0.25, 0.025, 4.0, 0.3, 1.0) * color.rgb;\r\n  material.alpha = length(flagColor);\r\n  material.diffuse = flagColor.rgb;\r\n  return material;\r\n}";
var CircleFadeMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  material.diffuse = 1.5 * color.rgb;\r\n  vec2 st = materialInput.st;\r\n  float dis = distance(st, vec2(0.5, 0.5));\r\n  float per = fract(czm_frameNumber * speed / 1000.0);\r\n  if(dis > per * 0.5){\r\n    material.alpha = color.a;\r\n  }else {\r\n    discard;\r\n  }\r\n  return material;\r\n}";
var CirclePulseMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st * 2.0 - 1.0;\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  float r = length(st) * 1.2;\r\n  float a = pow(r, 2.0);\r\n  float b = sin(r * 0.8 - 1.6);\r\n  float c = sin(r - 0.010);\r\n  float s = sin(a - time * 2.0 + b) * c;\r\n  float d = abs(1.0 / (s * 10.8)) - 0.01;\r\n  material.alpha = pow(d,10.0) ;\r\n  material.diffuse = color.rgb * d;\r\n  return material;\r\n}";
var CircleRingMaterial_default = "uniform vec4 color;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec2 center = st - vec2(0.5,0.5);\r\n  float length = length(center) / 0.5;\r\n  float time = 1. - abs(czm_frameNumber / 360. - 0.5);\r\n  float param = 1. - step(length, 0.6); \n  float scale = param * length; \n  float alpha = param * (1.0 - abs(scale - 0.8) / 0.2); \n  float param1 = step(length, 0.7); \n  float scale1 = param1 * length; \n  alpha += param1 * (1.0 - abs(scale1 - 0.35) / 0.35); \n  material.diffuse = color.rgb * vec3(color.a);\r\n  material.alpha = pow(alpha, 4.0);\r\n  return material;\r\n}";
var CircleRotateMaterial_default = "uniform vec4 color;\r\nuniform sampler2D image;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec2 center = st - vec2(0.5,0.5);\r\n  float time = -czm_frameNumber * 3.1415926 / 180.;\r\n  float sin_t = sin(time);\r\n  float cos_t = cos(time);\r\n  vec2 center_rotate = vec2(center.s * cos_t - center.t * sin_t + 0.5,center.s * sin_t + center.t * cos_t + 0.5);\r\n  vec4 colorImage = texture2D(image,center_rotate);\r\n  vec3 temp = colorImage.rgb * color.rgb;\r\n  temp *= color.a;\r\n  material.diffuse = temp;\r\n  float length = 2. - length(center) / 0.5;\r\n  material.alpha = colorImage.a * pow(length, 0.5);\r\n  return material;\r\n}";
var CircleScanMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nfloat circle(vec2 uv, float r, float blur) {\r\n  float d = length(uv) * 2.0;\r\n  float c = smoothstep(r+blur, r, d);\r\n  return c;\r\n}\n\nczm_material czm_getMaterial(czm_materialInput materialInput)\r\n{\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st - .5;\r\n  material.diffuse = color.rgb;\r\n  material.emission = vec3(0);\r\n  float t =fract(czm_frameNumber * speed / 1000.0);\r\n  float s = 0.3;\r\n  float radius1 = smoothstep(.0, s, t) * 0.5;\r\n  float alpha1 = circle(st, radius1, 0.01) * circle(st, radius1, -0.01);\r\n  float alpha2 = circle(st, radius1, 0.01 - radius1) * circle(st, radius1, 0.01);\r\n  float radius2 = 0.5 + smoothstep(s, 1.0, t) * 0.5;\r\n  float alpha3 = circle(st, radius1, radius2 + 0.01 - radius1) * circle(st, radius1, -0.01);\r\n  material.alpha = smoothstep(1.0, s, t) * (alpha1 + alpha2*0.1 + alpha3*0.1);\r\n  material.alpha *= color.a;\r\n  return material;\r\n}";
var CircleSpiralMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\n#define PI 3.14159265359\n\nvec2 rotate2D (vec2 _st, float _angle) {\r\n  _st =  mat2(cos(_angle),-sin(_angle),  sin(_angle),cos(_angle)) * _st;\r\n  return _st;\r\n}\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st * 2.0 - 1.0;\r\n  st *= 1.6;\r\n  float time = czm_frameNumber * speed / 1000.0;\r\n  float r = length(st);\r\n  float w = .3;\r\n  st = rotate2D(st,(r*PI*6.-time*2.));\r\n  float a = smoothstep(-w,.2,st.x) * smoothstep(w,.2,st.x);\r\n  float b = abs(1./(sin(pow(r,2.)*2.-time*1.3)*6.))*.4;\r\n  material.alpha = a * b ;\r\n  material.diffuse = color.rgb * a * b  * 3.0;\r\n  return material;\r\n}";
var CircleVaryMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st  * 2.0 - 1.0;\r\n  float time =czm_frameNumber * speed / 1000.0;\r\n  float radius = length(st);\r\n  float angle = atan(st.y/st.x);\r\n  float radius1 = sin(time * 2.0) + sin(40.0*angle+time)*0.01;\r\n  float radius2 = cos(time * 3.0);\r\n  vec3 fragColor = 0.2 + 0.5 * cos( time + color.rgb + vec3(0,2,4));\r\n  float inten1 = 1.0 - sqrt(abs(radius1 - radius));\r\n  float inten2 = 1.0 - sqrt(abs(radius2 - radius));\r\n  material.alpha = pow(inten1 + inten2 , 5.0) ;\r\n  material.diffuse = fragColor * (inten1 + inten2);\r\n  return material;\r\n}";
var CircleWaveMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nuniform float count;\r\nuniform float gradient;\n\nczm_material czm_getMaterial(czm_materialInput materialInput)\r\n{\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  material.diffuse = 1.5 * color.rgb;\r\n  vec2 st = materialInput.st;\r\n  float dis = distance(st, vec2(0.5, 0.5));\r\n  float per = fract(czm_frameNumber * speed / 1000.0);\r\n  if(count == 1.0){\r\n    if(dis > per * 0.5){\r\n      discard;\r\n    }else {\r\n      material.alpha = color.a  * dis / per / 2.0;\r\n    }\r\n  } else {\r\n    vec3 str = materialInput.str;\r\n    if(abs(str.z)  > 0.001){\r\n      discard;\r\n    }\r\n    if(dis > 0.5){\r\n      discard;\r\n    } else {\r\n      float perDis = 0.5 / count;\r\n      float disNum;\r\n      float bl = 0.0;\r\n      for(int i = 0; i <= 999; i++){\r\n        if(float(i) <= count){\r\n          disNum = perDis * float(i) - dis + per / count;\r\n          if(disNum > 0.0){\r\n            if(disNum < perDis){\r\n              bl = 1.0 - disNum / perDis;\r\n            }\r\n            else if(disNum - perDis < perDis){\r\n              bl = 1.0 - abs(1.0 - disNum / perDis);\r\n            }\r\n            material.alpha = pow(bl,(1.0 + 10.0 * (1.0 - gradient)));\r\n          }\r\n        }\r\n      }\r\n    }\r\n  }\r\n  return material;\r\n}";
Cesium.Material.CircleBlurType = "CircleBlur";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleBlurType, {
  fabric: {
    type: Cesium.Material.CircleBlurType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: CircleBlurMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleDiffuseType = "CircleDiffuse";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleDiffuseType, {
  fabric: {
    type: Cesium.Material.CircleDiffuseType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: CircleDiffuseMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleFadeType = "CircleFade";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleFadeType, {
  fabric: {
    type: Cesium.Material.CircleFadeType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: CircleFadeMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CirclePulseType = "CirclePulse";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CirclePulseType, {
  fabric: {
    type: Cesium.Material.CirclePulseType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 12
    },
    source: CirclePulseMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleRingType = "CircleRing";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleRingType, {
  fabric: {
    type: Cesium.Material.CircleRingType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7)
    },
    source: CircleRingMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleRotateType = "CircleRotate";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleRotateType, {
  fabric: {
    type: Cesium.Material.CircleRotateType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId
    },
    source: CircleRotateMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleScanType = "CircleScan";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleScanType, {
  fabric: {
    type: Cesium.Material.CircleScanType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 1
    },
    source: CircleScanMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleSpiralType = "CircleSpiral";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleSpiralType, {
  fabric: {
    type: Cesium.Material.CircleSpiralType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: CircleSpiralMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleVaryType = "CircleVary";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleVaryType, {
  fabric: {
    type: Cesium.Material.CircleVaryType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: CircleVaryMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CircleWaveType = "CircleWave";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CircleWaveType, {
  fabric: {
    type: Cesium.Material.CircleWaveType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3,
      count: 1,
      gradient: 0.1
    },
    source: CircleWaveMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
var CylinderFadeMaterial_default = "uniform vec4 color;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  float powerRatio = 1. / (fract(czm_frameNumber / 30.0) +  1.) ;\r\n  float alpha = pow(1. - st.t,powerRatio);\r\n  vec4 temp = vec4(color.rgb, alpha * color.a);\r\n  material.diffuse = temp.rgb;\r\n  material.alpha = temp.a;\r\n  return material;\r\n}";
var CylinderParticlesMaterial_default = "uniform vec4 color;\r\nuniform sampler2D image;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  float time = fract(czm_frameNumber / 90.) ;\r\n  vec2 new_st = fract(st- vec2(time,time));\r\n  vec4 colorImage = texture2D(image, new_st);\r\n  vec3 diffuse = colorImage.rgb;\r\n  float alpha = colorImage.a;\r\n  diffuse *= color.rgb;\r\n  alpha *= color.a;\r\n  material.diffuse = diffuse;\r\n  material.alpha = alpha * pow(1. - st.t,color.a);\r\n  return material;\r\n}";
Cesium.Material.CylinderFadeType = "CylinderFade";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CylinderFadeType, {
  fabric: {
    type: Cesium.Material.CylinderFadeType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7)
    },
    source: CylinderFadeMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.CylinderParticlesType = "CylinderParticles";
Cesium.Material._materialCache.addMaterial(Cesium.Material.CylinderParticlesType, {
  fabric: {
    type: Cesium.Material.CylinderParticlesType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId
    },
    source: CylinderParticlesMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
var EllipsoidElectricMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\n#define pi 3.1415926535\r\n#define PI2RAD 0.01745329252\r\n#define TWO_PI (2. * PI)\n\nfloat rands(float p){\r\n  return fract(sin(p) * 10000.0);\r\n}\n\nfloat noise(vec2 p){\r\n  float time = fract( czm_frameNumber * speed / 1000.0);\r\n  float t = time / 20000.0;\r\n  if(t > 1.0) t -= floor(t);\r\n  return rands(p.x * 14. + p.y * sin(t) * 0.5);\r\n}\n\nvec2 sw(vec2 p){\r\n  return vec2(floor(p.x), floor(p.y));\r\n}\n\nvec2 se(vec2 p){\r\n  return vec2(ceil(p.x), floor(p.y));\r\n}\n\nvec2 nw(vec2 p){\r\n  return vec2(floor(p.x), ceil(p.y));\r\n}\n\nvec2 ne(vec2 p){\r\n  return vec2(ceil(p.x), ceil(p.y));\r\n}\n\nfloat smoothNoise(vec2 p){\r\n  vec2 inter = smoothstep(0.0, 1.0, fract(p));\r\n  float s = mix(noise(sw(p)), noise(se(p)), inter.x);\r\n  float n = mix(noise(nw(p)), noise(ne(p)), inter.x);\r\n  return mix(s, n, inter.y);\r\n}\n\nfloat fbm(vec2 p){\r\n  float z = 2.0;\r\n  float rz = 0.0;\r\n  vec2 bp = p;\r\n  for(float i = 1.0; i < 6.0; i++){\r\n    rz += abs((smoothNoise(p) - 0.5)* 2.0) / z;\r\n    z *= 2.0;\r\n    p *= 2.0;\r\n  }\r\n  return rz;\r\n}\n\nczm_material czm_getMaterial(czm_materialInput materialInput)\r\n{\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec2 st2 = materialInput.st;\r\n  float time = fract( czm_frameNumber * speed / 1000.0);\r\n  if (st.t < 0.5) {\r\n    discard;\r\n  }\r\n  st *= 4.;\r\n  float rz = fbm(st);\r\n  st /= exp(mod( time * 2.0, pi));\r\n  rz *= pow(15., 0.9);\r\n  vec4 temp = vec4(0);\r\n  temp = mix( color / rz, vec4(color.rgb, 0.1), 0.2);\r\n  if (st2.s < 0.05) {\r\n    temp = mix(vec4(color.rgb, 0.1), temp, st2.s / 0.05);\r\n  }\r\n  if (st2.s > 0.95){\r\n    temp = mix(temp, vec4(color.rgb, 0.1), (st2.s - 0.95) / 0.05);\r\n  }\r\n  material.diffuse = temp.rgb;\r\n  material.alpha = temp.a * 2.0;\r\n  return material;\r\n}";
var EllipsoidTrailMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  float alpha = abs(smoothstep(0.5,1.,fract( -st.t - time)));\r\n  alpha += .1;\r\n  material.alpha = alpha;\r\n  material.diffuse = color.rgb;\r\n  return material;\r\n}";
Cesium.Material.EllipsoidElectricType = "EllipsoidElectric";
Cesium.Material._materialCache.addMaterial(Cesium.Material.EllipsoidElectricType, {
  fabric: {
    type: Cesium.Material.EllipsoidElectricType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 1
    },
    source: EllipsoidElectricMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.EllipsoidTrailType = "EllipsoidTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.EllipsoidTrailType, {
  fabric: {
    type: Cesium.Material.EllipsoidTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: EllipsoidTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
var PolylineFlickerMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  float time = fract( czm_frameNumber  *  speed / 1000.0);\r\n  vec2 st = materialInput.st;\r\n  float scalar = smoothstep(0.0,1.0,time);\r\n  material.diffuse = color.rgb * scalar;\r\n  material.alpha = color.a * scalar ;\r\n  return material;\r\n}";
var PolylineFlowMaterial_default = "uniform vec4 color;\r\nuniform float speed;\r\nuniform float percent;\r\nuniform float gradient;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  float t =fract(czm_frameNumber * speed / 1000.0);\r\n  t *= (1.0 + percent);\r\n  float alpha = smoothstep(t- percent, t, st.s) * step(-t, -st.s);\r\n  alpha += gradient;\r\n  material.diffuse = color.rgb;\r\n  material.alpha = alpha;\r\n  return material;\r\n}";
var PolylineImageTrailMaterial_default = "uniform sampler2D image;\r\nuniform float speed;\r\nuniform vec4 color;\r\nuniform vec2 repeat;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n   czm_material material = czm_getDefaultMaterial(materialInput);\r\n   vec2 st = repeat * materialInput.st;\r\n   float time = fract(czm_frameNumber * speed / 1000.0);\r\n   vec4 colorImage = texture2D(image, vec2(fract(st.s - time), st.t));\r\n   if(color.a == 0.0){\r\n     if(colorImage.rgb == vec3(1.0) || colorImage.rgb == vec3(0.0)){\r\n       discard;\r\n     }\r\n    material.alpha = colorImage.a;\r\n    material.diffuse = colorImage.rgb;\r\n   }else{\r\n    material.alpha = colorImage.a * color.a;\r\n    material.diffuse = max(color.rgb * material.alpha * 3.0, color.rgb);\r\n   }\r\n   return material;\r\n}";
var PolylineLightingMaterial_default = "uniform sampler2D image;\r\nuniform vec4 color;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec4 colorImage = texture2D(image,st);\r\n  vec3 fragColor = color.rgb;\r\n  material.alpha = colorImage.a * color.a * 3.;\r\n  material.diffuse = max(fragColor.rgb  +  colorImage.rgb , fragColor.rgb);\r\n  return material;\r\n}";
var PolylineLightingTrailMaterial_default = "uniform sampler2D image;\r\nuniform vec4 color;\r\nuniform float speed;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  vec4 colorImage = texture2D(image,st);\r\n  vec3 fragColor = color.rgb;\r\n  if(st.t > 0.45 && st.t < 0.55 ) {\r\n    fragColor = vec3(1.0);\r\n  }\r\n  if(color.a == 0.0){\r\n    material.alpha = colorImage.a * 1.5 * fract(st.s - time);\r\n    material.diffuse = colorImage.rgb;\r\n  }else{\r\n    material.alpha = colorImage.a * color.a * 1.5 * smoothstep(.0,1., fract(st.s - time));\r\n    material.diffuse = max(fragColor.rgb * material.alpha , fragColor.rgb);\r\n  }\r\n  return material;\r\n}";
var PolylineTrailMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n   czm_material material = czm_getDefaultMaterial(materialInput);\r\n   vec2 st = materialInput.st;\r\n   float time = fract(czm_frameNumber * speed / 1000.0);\r\n   material.diffuse = color.rgb;\r\n   material.alpha = color.a * fract(st.s-time);\r\n   return material;\r\n}";
Cesium.Material.PolylineFlickerType = "PolylineFlicker";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineFlickerType, {
  fabric: {
    type: Cesium.Material.PolylineFlickerType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 1
    },
    source: PolylineFlickerMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.PolylineFlowType = "PolylineFlow";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineFlowType, {
  fabric: {
    type: Cesium.Material.PolylineFlowType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 1,
      percent: 0.03,
      gradient: 0.1
    },
    source: PolylineFlowMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.PolylineImageTrailType = "PolylineImageTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineImageTrailType, {
  fabric: {
    type: Cesium.Material.PolylineImageTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId,
      speed: 1,
      repeat: new Cesium.Cartesian2(1, 1)
    },
    source: PolylineImageTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.PolylineLightingType = "PolylineLighting";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineLightingType, {
  fabric: {
    type: Cesium.Material.PolylineLightingType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId
    },
    source: PolylineLightingMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.PolylineLightingTrailType = "PolylineLightingTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineLightingTrailType, {
  fabric: {
    type: Cesium.Material.PolylineLightingTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId,
      speed: 3
    },
    source: PolylineLightingTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.PolylineTrailType = "PolylineTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineTrailType, {
  fabric: {
    type: Cesium.Material.PolylineTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId,
      speed: 1,
      repeat: new Cesium.Cartesian2(1, 1)
    },
    source: PolylineTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
var RadarLineMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st * 2.0 - 1.0;\r\n  float t = czm_frameNumber * speed / 1000.0 ;\r\n  vec3 col = vec3(0.0);\r\n  vec2 p = vec2(sin(t), cos(t));\r\n  float d = length(st - dot(p, st) * p);\r\n  if (dot(st, p) < 0.) {\r\n    d = length(st);\r\n  }\n\n  col = .006 / d * color.rgb;\n\n  if(distance(st,vec2(0)) >  0.99 ){\r\n    col =color.rgb;\r\n  }\n\n  material.alpha  = pow(length(col),2.0);\r\n  material.diffuse = col * 3.0 ;\r\n  return material;\r\n}";
var RadarSweepMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\n#define PI 3.14159265359\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec2 scrPt = st * 2.0 - 1.0;\r\n  float time = czm_frameNumber * speed / 1000.0 ;\r\n  vec3 col = vec3(0.0);\r\n  mat2 rot;\r\n  float theta = -time * 1.0 * PI - 2.2;\r\n  float cosTheta, sinTheta;\r\n  cosTheta = cos(theta);\r\n  sinTheta = sin(theta);\r\n  rot[0][0] = cosTheta;\r\n  rot[0][1] = -sinTheta;\r\n  rot[1][0] = sinTheta;\r\n  rot[1][1] = cosTheta;\r\n  vec2 scrPtRot = rot * scrPt;\r\n  float angle = 1.0 - (atan(scrPtRot.y, scrPtRot.x) / 6.2831 + 0.5);\r\n  float falloff = 1.0 - length(scrPtRot);\r\n  float ringSpacing = 0.23;\r\n  if(mod(length(scrPtRot), ringSpacing) < 0.015 && length(scrPtRot) / ringSpacing < 5.0) {\r\n    col += vec3(0, 0.5, 0);\r\n  }\r\n  col += vec3(0, 0.8, 0) * step(mod(length(scrPtRot), ringSpacing), 0.01) * step(length(scrPtRot), 1.0);\r\n  material.alpha =pow(length(col + vec3(.5)),5.0);\r\n  material.diffuse =  (0.5 +  pow(angle, 2.0) * falloff ) *   color.rgb    ;\r\n  return material;\r\n}";
var RadarWaveMaterial_default = "uniform vec4 color;\r\nuniform float speed;\n\n#define PI 3.14159265359\n\nfloat rand(vec2 co){\r\n  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);\r\n}\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  vec2 pos = st - vec2(0.5);\r\n  float time = czm_frameNumber * speed / 1000.0 ;\r\n  float r = length(pos);\r\n  float t = atan(pos.y, pos.x) - time * 2.5;\r\n  float a = (atan(sin(t), cos(t)) + PI)/(2.0*PI);\r\n  float ta = 0.5;\r\n  float v = smoothstep(ta-0.05,ta+0.05,a) * smoothstep(ta+0.05,ta-0.05,a);\r\n  vec3 flagColor = color.rgb * v;\r\n  float blink = pow(sin(time*1.5)*0.5+0.5, 0.8);\r\n  flagColor = color.rgb *  pow(a, 8.0*(.2+blink))*(sin(r*500.0)*.5+.5) ;\r\n  flagColor = flagColor * pow(r, 0.4);\r\n  material.alpha = length(flagColor) * 1.3;\r\n  material.diffuse = flagColor * 3.0;\r\n  return material;\r\n}";
Cesium.Material.RadarLineType = "RadarLine";
Cesium.Material._materialCache.addMaterial(Cesium.Material.RadarLineType, {
  fabric: {
    type: Cesium.Material.RadarLineType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: RadarLineMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.RadarSweepType = "RadarSweep";
Cesium.Material._materialCache.addMaterial(Cesium.Material.RadarSweepType, {
  fabric: {
    type: Cesium.Material.RadarSweepType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: RadarSweepMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.RadarWaveType = "RadarWave";
Cesium.Material._materialCache.addMaterial(Cesium.Material.RadarWaveType, {
  fabric: {
    type: Cesium.Material.RadarWaveType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3
    },
    source: RadarWaveMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
var WallDiffuseMaterial_default = "uniform vec4 color;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st;\r\n  material.diffuse = color.rgb * 2.0;\r\n  material.alpha = color.a * (1.0-fract(st.t)) * 0.8;\r\n  return material;\r\n}";
var WallImageTrailMaterial_default = "uniform sampler2D image;\r\nuniform vec4 color;\r\nuniform float speed;\r\nuniform vec2 repeat;\r\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  vec2 st = materialInput.st * repeat;\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  vec4 colorImage = texture2D(image, vec2(fract(st.s - time), st.t));\r\n  material.alpha =  colorImage.a * color.a ;\r\n  material.diffuse = colorImage.rgb * color.rgb * 3.0 ;\r\n  return material;\r\n}";
var WallLineTrailMaterial_default = "uniform sampler2D image;\r\nuniform float speed;\r\nuniform vec4 color;\r\nuniform vec2 repeat;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n  czm_material material = czm_getDefaultMaterial(materialInput);\r\n  float perDis = 1.0 / repeat.y / 3.0  ;\r\n  vec2 st = materialInput.st * repeat;\r\n  float time = fract(czm_frameNumber * speed / 1000.0);\r\n  vec4 colorImage = texture2D(image, vec2(st.s, fract(st.t - time)));\r\n  material.alpha =  colorImage.a * smoothstep(.2 ,1. ,distance(st.t * perDis ,1. + perDis ));\r\n  material.diffuse = max(color.rgb * material.alpha * 1.5, color.rgb);\r\n  material.emission = max(color.rgb * material.alpha * 1.5, color.rgb);\r\n  return material;\r\n}";
var WallTrailMaterial_default = "uniform sampler2D image;\r\n uniform float speed;\r\n uniform vec4 color;\n\nczm_material czm_getMaterial(czm_materialInput materialInput){\r\n   czm_material material = czm_getDefaultMaterial(materialInput);\r\n   vec2 st = materialInput.st;\r\n   float time = fract(czm_frameNumber * speed / 1000.0);\r\n   vec4 colorImage = texture2D(image, vec2(fract(st.t - time), st.t));\r\n   if(color.a == 0.0){\r\n    material.alpha = colorImage.a;\r\n    material.diffuse = colorImage.rgb;\r\n   }else{\r\n    material.alpha = colorImage.a * color.a;\r\n    material.diffuse = max(color.rgb * material.alpha * 3.0, color.rgb);\r\n   }\r\n   return material;\r\n}";
Cesium.Material.WallDiffuseType = "WallDiffuse";
Cesium.Material._materialCache.addMaterial(Cesium.Material.WallDiffuseType, {
  fabric: {
    type: Cesium.Material.WallDiffuseType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7)
    },
    source: WallDiffuseMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.WallImageTrailType = "WallImageTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.WallImageTrailType, {
  fabric: {
    type: Cesium.Material.WallImageTrailType,
    uniforms: {
      image: Cesium.Material.DefaultImageId,
      color: new Cesium.Color(1, 0, 0, 0.7),
      speed: 3,
      repeat: new Cesium.Cartesian2(1, 1)
    },
    source: WallImageTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.WallLineTrailType = "WallLineTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.WallLineTrailType, {
  fabric: {
    type: Cesium.Material.WallLineTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId,
      repeat: new Cesium.Cartesian2(1, 1),
      speed: 3
    },
    source: WallLineTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
Cesium.Material.WallTrailType = "WallTrail";
Cesium.Material._materialCache.addMaterial(Cesium.Material.WallTrailType, {
  fabric: {
    type: Cesium.Material.WallTrailType,
    uniforms: {
      color: new Cesium.Color(1, 0, 0, 0.7),
      image: Cesium.Material.DefaultImageId,
      speed: 1
    },
    source: WallTrailMaterial_default
  },
  translucent: function(material) {
    return true;
  }
});
class MaterialProperty {
  constructor(options = {}) {
    this._definitionChanged = new Cesium.Event();
    this._color = void 0;
    this._colorSubscription = void 0;
    this._speed = void 0;
    this._speedSubscription = void 0;
    this.color = options.color || Cesium.Color.fromBytes(0, 255, 255, 255);
    this.speed = options.speed || 1;
  }
  get isConstant() {
    return false;
  }
  get definitionChanged() {
    return this._definitionChanged;
  }
  getType(time) {
    return null;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    return result;
  }
  equals(other) {
    return this === other;
  }
}
class CircleBlurMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleBlurType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleBlurMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleBlurMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleDiffuseMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleDiffuseType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleDiffuseMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleDiffuseMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleFadeMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleFadeType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleFadeMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleFadeMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CirclePulseMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CirclePulseType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CirclePulseMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CirclePulseMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleScanMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleScanType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleScanMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleScanMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleSpiralMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleSpiralType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleSpiralMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleSpiralMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleVaryMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.CircleVaryType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleVaryMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleVaryMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class CircleWaveMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
    this.count = Math.max(options.count || 3, 1);
    this.gradient = Cesium.Math.clamp(options.gradient || 0.1, 0, 1);
  }
  get isConstant() {
    return false;
  }
  get definitionChanged() {
    return this._definitionChanged;
  }
  getType(time) {
    return Cesium.Material.CircleWaveType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    result.count = this.count;
    result.gradient = this.gradient;
    return result;
  }
  equals(other) {
    return this === other || other instanceof CircleWaveMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(CircleWaveMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class EllipsoidElectricMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.EllipsoidElectricType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof EllipsoidElectricMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(EllipsoidElectricMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class EllipsoidTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.EllipsoidTrailType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof EllipsoidTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(EllipsoidTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class PolylineFlickerMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.PolylineFlickerType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineFlickerMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(PolylineFlickerMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class PolylineFlowMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
    this._percent = void 0;
    this._percentSubscription = void 0;
    this._gradient = void 0;
    this._gradientSubscription = void 0;
    this.percent = options.percent || 0.03;
    this.gradient = options.gradient || 0.1;
  }
  getType(time) {
    return Cesium.Material.PolylineFlowType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    result.percent = this._percent;
    result.gradient = this._gradient;
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineFlowMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed) && Cesium.Property.equals(this._percent, other._percent) && Cesium.Property.equals(this._gradient, other._gradient);
  }
}
Object.defineProperties(PolylineFlowMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed"),
  percent: Cesium.createPropertyDescriptor("percent"),
  gradient: Cesium.createPropertyDescriptor("gradient")
});
class PolylineImageTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    var _a, _b;
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this._repeat = void 0;
    this._repeatSubscription = void 0;
    this.image = options.image;
    this.repeat = new Cesium.Cartesian2(((_a = options.repeat) == null ? void 0 : _a.x) || 1, ((_b = options.repeat) == null ? void 0 : _b.y) || 1);
  }
  getType(time) {
    return Cesium.Material.PolylineImageTrailType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    result.repeat = Cesium.Property.getValueOrUndefined(this._repeat, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineImageTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._image, other._image) && Cesium.Property.equals(this._repeat, other._repeat) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(PolylineImageTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed"),
  image: Cesium.createPropertyDescriptor("image"),
  repeat: Cesium.createPropertyDescriptor("repeat")
});
var IMG$2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAACYCAYAAACS0lH9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAJ0GlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgMTE2LjE2NDY1NSwgMjAyMS8wMS8yNi0xNTo0MToyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDplODY0YmNmNy1lZGIyLWIyNDQtYWI0NC04OWZkNmMwOTQ4MDYiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjIyOGMxMDUtODFmZS00MjAxLWIwOTEtZDkwMGI0NTI0NWMwIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IjcxNzA5OEJGODAwODNEREJGRDQyQzAzMzQ5NDlDRDFDIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9IiIgdGlmZjpJbWFnZVdpZHRoPSI1MTIiIHRpZmY6SW1hZ2VMZW5ndGg9IjE1MiIgdGlmZjpQaG90b21ldHJpY0ludGVycHJldGF0aW9uPSIyIiB0aWZmOlNhbXBsZXNQZXJQaXhlbD0iMyIgdGlmZjpYUmVzb2x1dGlvbj0iMS8xIiB0aWZmOllSZXNvbHV0aW9uPSIxLzEiIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjEiIGV4aWY6RXhpZlZlcnNpb249IjAyMzEiIGV4aWY6Q29sb3JTcGFjZT0iNjU1MzUiIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSI1MTIiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSIxNTIiIHhtcDpDcmVhdGVEYXRlPSIyMDIxLTAyLTIzVDEwOjAyOjQxKzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMS0wMi0yM1QxMDowODo0NCswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMS0wMi0yM1QxMDowODo0NCswODowMCI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmZmYTk5ZjhhLTdiZmQtNDcxNi04MTgwLWJmZTUyMmFmNGUzNSIgc3RFdnQ6d2hlbj0iMjAyMS0wMi0yM1QxMDowODo0NCswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNvbnZlcnRlZCIgc3RFdnQ6cGFyYW1ldGVycz0iZnJvbSBpbWFnZS9qcGVnIHRvIGltYWdlL3BuZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iZGVyaXZlZCIgc3RFdnQ6cGFyYW1ldGVycz0iY29udmVydGVkIGZyb20gaW1hZ2UvanBlZyB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjYyMjhjMTA1LTgxZmUtNDIwMS1iMDkxLWQ5MDBiNDUyNDVjMCIgc3RFdnQ6d2hlbj0iMjAyMS0wMi0yM1QxMDowODo0NCswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmZmYTk5ZjhhLTdiZmQtNDcxNi04MTgwLWJmZTUyMmFmNGUzNSIgc3RSZWY6ZG9jdW1lbnRJRD0iNzE3MDk4QkY4MDA4M0REQkZENDJDMDMzNDk0OUNEMUMiIHN0UmVmOm9yaWdpbmFsRG9jdW1lbnRJRD0iNzE3MDk4QkY4MDA4M0REQkZENDJDMDMzNDk0OUNEMUMiLz4gPHBob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPHJkZjpCYWc+IDxyZGY6bGk+NzE3MDk4QkY4MDA4M0REQkZENDJDMDMzNDk0OUNEMUM8L3JkZjpsaT4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPHRpZmY6Qml0c1BlclNhbXBsZT4gPHJkZjpTZXE+IDxyZGY6bGk+ODwvcmRmOmxpPiA8cmRmOmxpPjg8L3JkZjpsaT4gPHJkZjpsaT44PC9yZGY6bGk+IDwvcmRmOlNlcT4gPC90aWZmOkJpdHNQZXJTYW1wbGU+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+h9EbuwAAA01JREFUeJzt3TuOFVkQBNDoJhmEkDBgZv8L5GMgIcRvHg5ryDTiHKn98Co67q16T4/HYwIAVHm+DgAA7FMAAKCQAgAAhSbJ03UIAGCXBQAACikAAFDIEQAAFLIAAEAhBQAACikAAFBoogQAQB0PfwAopAAAQCGvAQJAIQsAABSyAABAIQsAABSyAABAIQUAAAo5AgCAQr4ECACFPPwBoJACAACFHAEAQCEPfwAoZAEAgEIe/gBQyAIAAIU8/AGgkAUAAAr5LQAAKOS/fwAoZAEAgELuAABAIQsAABRSAACgkAIAAIXcAQCAQhYAAChkAQCAQgoAABRSAACgkIc/ABSaJC+uQwAAu7wFAACFFAAAKDR//wCAIhYAACjkEiAAFLIAAEChSfLyOgQAsMuXAAGgkLcAAKCQAgAAhbwFAACFFAAAKOQIAAAKTZJ/rkMAALsUAAAo5AgAAAr5EiAAFFIAAKCQOwAAUEgBAIBCk+TVdQgAYNckeXMdAgDYNUneXocAAHZNkvfXIQCAXZPkv+sQAMCuSfLvdQgAYNckeXcdAgDY5RIgABSaJK+vQwAAu/wWAAAUmiRP1yEAgF2T5HEdAgDYNUl+XocAAHZNkm/XIQCAXZPky3UIAGDXJPl8HQIA2DVJPl6HAAB2TZIP1yEAgF2T5NN1CABgl0uAAFBokny9DgEA7Jok369DAAC7JsmP6xAAwC4FAAAK+S0AACikAABAoUny6zoEALDLHQAAKKQAAEAhRwAAUGiS/L4OAQDsUgAAoJAjAAAopAAAQKFJ8v91CABgly8BAkChSfK4DgEA7PIWAAAUsgAAQCFvAQBAIQsAABRSAACgkEuAAFDo+ToAALDPlwABoJACAACFFAAAKOQtAAAoZAEAgEIWAAAopAAAQCEFAAAKuQMAAIUsAABQyKeAAaCQBQAACrkDAACFHAEAQCELAAAUsgAAQCELAAAUsgAAQCELAAAUsgAAQCEFAAAKOQIAgEIWAAAo5LcAAKCQAgAAhRwBAEAhCwAAFLIAAEAhCwAAFLIAAEAhBQAACvkSIAAUsgAAQCEFAAAKKQAAUMhrgABQyAIAAIUUAAAo5AgAAApZAACgkAIAAIUUAAAo5A4AABT6A6gaPQ6/wRIfAAAAAElFTkSuQmCC";
class PolylineLightingMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this.image = IMG$2;
  }
  getType(time) {
    return Cesium.Material.PolylineLightingType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineLightingMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._image, other._image);
  }
}
Object.defineProperties(PolylineLightingMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  image: Cesium.createPropertyDescriptor("image")
});
class PolylineLightingTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this.image = IMG$2;
  }
  getType(time) {
    return Cesium.Material.PolylineLightingTrailType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineLightingTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(PolylineLightingTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed"),
  image: Cesium.createPropertyDescriptor("image")
});
class PolylineTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.PolylineTrailType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof PolylineTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(PolylineTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class RadarLineMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.RadarLineType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof RadarLineMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(RadarLineMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class RadarSweepMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.RadarSweepType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof RadarSweepMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(RadarSweepMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class RadarWaveMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
  }
  getType(time) {
    return Cesium.Material.RadarWaveType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof RadarWaveMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(RadarWaveMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed")
});
class WallImageTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    var _a, _b;
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this._repeat = void 0;
    this._repeatSubscription = void 0;
    this.image = options.image;
    this.repeat = new Cesium.Cartesian2(((_a = options.repeat) == null ? void 0 : _a.x) || 1, ((_b = options.repeat) == null ? void 0 : _b.y) || 1);
  }
  getType(time) {
    return Cesium.Material.WallImageTrailType;
  }
  getValue(time, result) {
    result = Cesium.defaultValue(result, {});
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    result.repeat = Cesium.Property.getValueOrUndefined(this._repeat, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof WallImageTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._image, other._image) && Cesium.Property.equals(this._repeat, other._repeat) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(WallImageTrailMaterialProperty.prototype, {
  image: Cesium.createPropertyDescriptor("image"),
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed"),
  repeat: Cesium.createPropertyDescriptor("repeat")
});
var IMG$1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVwAAABACAYAAABWdc94AAAACXBIWXMAAAsTAAALEwEAmpwYAAAGx2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgMTE2LjE2NDY1NSwgMjAyMS8wMS8yNi0xNTo0MToyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiIHhtcDpDcmVhdGVEYXRlPSIyMDIxLTAyLTIzVDE3OjE0OjMyKzA4OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIxLTAyLTI0VDE0OjIwOjE2KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMS0wMi0yNFQxNDoyMDoxNiswODowMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1NzQzY2I0NC0zMzk3LTQ5OTAtYjg4OC0yNDFlNmExYmQyYWYiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDo5YWYxZDY1MC1jNWRlLTVmNDgtYWYzNi1hZDE4ZWRkN2QzYTAiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpiMmZjZmU2Zi1hZWQwLTRjMWQtYjZmOS1lNjAwMjJiNmEwOGUiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBwaG90b3Nob3A6SUNDUHJvZmlsZT0ic1JHQiBJRUM2MTk2Ni0yLjEiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmIyZmNmZTZmLWFlZDAtNGMxZC1iNmY5LWU2MDAyMmI2YTA4ZSIgc3RFdnQ6d2hlbj0iMjAyMS0wMi0yM1QxNzoxNDozMiswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjNjM2JjM2I5LTkwNDEtNDk1ZS04MTc5LTdkZjc3NDIwZDczOSIgc3RFdnQ6d2hlbj0iMjAyMS0wMi0yM1QxNzoxNDozMiswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjU3NDNjYjQ0LTMzOTctNDk5MC1iODg4LTI0MWU2YTFiZDJhZiIgc3RFdnQ6d2hlbj0iMjAyMS0wMi0yNFQxNDoyMDoxNiswODowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjIgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+eAiLkwAAAQZJREFUeJzt3LERwDAMxDAl5/1XdqYIXRiYQBULFf/svQeA/72nDwC4heACRAQXICK4ABHBBYgILkBEcAEiggsQEVyAiOACRAQXICK4ABHBBYgILkBkzYx9RoDAmpnn9BEAN/BSAIgILkBEcAEiggsQEVyAiOACRAQXICK4ABHBBYgILkBEcAEiggsQEVyAiOACRAQXIGKAHCBigBwg4qUAEBFcgIjgAkQEFyAiuAARwQWICC5ARHABIoILEBFcgIjgAkQEFyAiuAARwQWI2MMFiNjDBYh4KQBEBBcgIrgAEcEFiAguQERwASKCCxARXICI4AJEBBcgIrgAEcEFiAguQERwASIfRmAGiWTgoMYAAAAASUVORK5CYII=";
class WallLineTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    var _a, _b;
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this._repeat = void 0;
    this._repeatSubscription = void 0;
    this.image = IMG$1;
    this.repeat = new Cesium.Cartesian2(((_a = options.repeat) == null ? void 0 : _a.x) || 1, ((_b = options.repeat) == null ? void 0 : _b.y) || 1);
  }
  getType(time) {
    return Cesium.Material.WallLineTrailType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    result.repeat = Cesium.Property.getValueOrUndefined(this._repeat, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof WallLineTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed) && Cesium.Property.equals(this._repeat, other._repeat);
  }
}
Object.defineProperties(WallLineTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  image: Cesium.createPropertyDescriptor("image"),
  repeat: Cesium.createPropertyDescriptor("repeat"),
  speed: Cesium.createPropertyDescriptor("speed")
});
var IMG = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAOhlWElmTU0AKgAAAAgABgESAAMAAAABAAEAAAEaAAUAAAABAAAAVgEbAAUAAAABAAAAXgExAAIAAAAkAAAAZgEyAAIAAAAUAAAAiodpAAQAAAABAAAAngAAAAAAAABIAAAAAQAAAEgAAAABQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKE1hY2ludG9zaCkAMjAxODoxMDoyNiAxNTozMDozNAAABJAEAAIAAAAUAAAA1KABAAMAAAABAAEAAKACAAQAAAABAAAAQKADAAQAAAABAAAAQAAAAAAyMDE4OjEwOjI2IDE1OjI0OjI1ALUCxicAAAAJcEhZcwAACxMAAAsTAQCanBgAAAdgaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgICAgICAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6MjgyZGYxNWEtYzg5MC00ODUzLWJlZDQtOWEyZjQxMmY0NjljPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8eG1wTU06RG9jdW1lbnRJRD54bXAuZGlkOjI4MmRmMTVhLWM4OTAtNDg1My1iZWQ0LTlhMmY0MTJmNDY5YzwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD54bXAuZGlkOjI4MmRmMTVhLWM4OTAtNDg1My1iZWQ0LTlhMmY0MTJmNDY5YzwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoTWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxOC0xMC0yNlQxNToyNDoyNSswODowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDoyODJkZjE1YS1jODkwLTQ4NTMtYmVkNC05YTJmNDEyZjQ2OWM8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y3JlYXRlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICA8L3htcE1NOkhpc3Rvcnk+CiAgICAgICAgIDx4bXA6TW9kaWZ5RGF0ZT4yMDE4LTEwLTI2VDE1OjMwOjM0KzA4OjAwPC94bXA6TW9kaWZ5RGF0ZT4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoTWFjaW50b3NoKTwveG1wOkNyZWF0b3JUb29sPgogICAgICAgICA8eG1wOk1ldGFkYXRhRGF0ZT4yMDE4LTEwLTI2VDE1OjMwOjM0KzA4OjAwPC94bXA6TWV0YWRhdGFEYXRlPgogICAgICAgICA8eG1wOkNyZWF0ZURhdGU+MjAxOC0xMC0yNlQxNToyNDoyNSswODowMDwveG1wOkNyZWF0ZURhdGU+CiAgICAgICAgIDxwaG90b3Nob3A6SUNDUHJvZmlsZT5zUkdCIElFQzYxOTY2LTIuMTwvcGhvdG9zaG9wOklDQ1Byb2ZpbGU+CiAgICAgICAgIDxwaG90b3Nob3A6Q29sb3JNb2RlPjM8L3Bob3Rvc2hvcDpDb2xvck1vZGU+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9kYzpmb3JtYXQ+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgr82RRBAAAQ+0lEQVR4AZWaWZIkNw5Ea5M00khnmi/d/zgyLV017znhTEZWdk8PzCIAAg4QABlLRtXz77///p8//vjjy/v7+5ePj48vf/755xfo/eXl5f2ff/55R/f8NPTDDz9s+dRrfnt7i+3Un/Jpf319feF4lj8/P78yV8bKhHplnpcV8u0Fv1cOx8BxAPrjjz+GL7fXYEZ+09c4Hsj6Nubrzz///C+OX3799dd///LLL7/+9ttvP+tM7O+jv/766xPQIj3+/vvvJ4/SWXx195x4u2knnsbvRutz2hyzQNFRtLiXsdukJxZSbhOelQ9b9NqKwfbyRrAPJkT++MDwgV8OivlggsiYkpA29JHprvN9ImJV90FzdyHVE0v7OwfhX96Z/1nu2ByUWWh1FmBzYe8WtHNTVm+jyCMLoMLA6mHRibMm4rmTE18ddsfyjxcbMMon+XnYGI9TV9kGeXQsJ2BiyKXqTrn46s6xOslmNS+5RahvPHQOQ9jCLUga9SemTWzxBeQSwLZXSkMLK4ixSV2SaOJthNgmWF7/8nu98xpbOm1elto8LF5b7eWfCnnJFdCpwvG/NOSM0zlzCbjtwL5zA0y3BXblTaKTtgk4X7a3wVqINrflxLAIzfsmObGeWcUsHXO72t4QnTvb31U3JwiWS2E3QX9tcCsOB8c0WWG3u80Tn3mVI8wJrHZH2RG5ttxyJSfwuB+fOm02w6KLL9fmrpCfuqN5ezeJkcQ1B7nj6gP4ysk6a6oMX9UpPKDGtRHS3jctJto5NZFTV9kb3NduhGLOJ0bjnE1onHv+PU8l6tqF3/t3zC6JyNyfsPrXnpugyCbZAOVf09d+z4u/58VV77gNOS6fwvZNcCsOwRg9DvWuwYV/pB/dxfbmIwdDrmnuAX2kPLsSXps63Xfc1Z9g23ZilHlZ+WTTz9yKbRw5hwXs+RyLG5XPzDwanVd5bDsP9eqql6Pa8QwkqVtMD+5NAm2C3ETaBECoVsBHW12bAcq98Z1j9LEZs3p1kjr9nJdrcc+PKS8vbk/mh+V9JoWLr0/1jhNwTuo5doWVz4oPWfG5b0pnIbuTJnpffBMpd6Ufkfaz+HtMm1BM7eR02aKdp/ZH4xZejFzcOa7O+BZeW97BO2jwrmb15Q1arl5ZfH3LW9iJFd/CT3195nL0pX/H00eySPmZ/Oj3gmkTN/yCre6cF93TGw4vFPDGjegfktt3b4GY8qw4nSq3QJOQqleurTp5d5LyJOPUZhuO3R8wLoi37/yIIU50/vhBpy1j3cRB+VEELjE8Jfhx2gYEYwiBp1Hyt0ng6aeffnrh0eWLhEHIM+/ll8JMHqcQclc/L0Fn0fOycSm6z11w+cEyBSQ/i8ee5MzHgpikY8vZhU0BaZwGSaxcm9Tx1BKdJ8bZFcXB1w5QoCDsr/v6Q/aHyqVIV7FNKDcYzdv6dGdO2jouft763HW5/4ApX1tBp2mCIoer3CKyQxjf8+2rTbCcucOpJf6MdyBtHjT7zQnyI8HVIdH80jCATQCUa5II4Y5PamHaJWJ8KtoEtBVrbCjJTa7Z9uI4srdHv78FTJ57yxMuTZBLuinjZwgpek+jkxeHuCiXAI5GMUHf47OlfUkRos3tqx1+2dbaStO8S6FtRgs3HInlBofchMPVm7WFalMejGKSd7zqWX1i7hSkfQzlQIPVLthxOLkk1sRZO6CJznt4Hl8AqDe/MhynzhbioD4xzIk5ssrlxNhjISZiDLmZgpNJNi6rrY7YbQrQfD3KyoPrjmjhbVhukviGEpGTcSSLljCGOX/JN0GD7JUD4aexvKC44v+r6NNukcY6dU6m/tBlblRd1SynyYGNTo4dditeOdlPg5Bvezy1rV1j0cYq3cXdu0E9x/NuAAkmSXeByc6Enx6DFiMxQQpbo1uB+G39xEm8owHZCZNAboaVTQh/l8fiIsOz+qotigI7FpqdYjoOIItKY6YR7t7upjQGextkvNUAHPcqAfjwTr12/9quLfLkLcgg1atjwnO1HzbKJHwK6Kc8yWbVLfKwpyjzQ53tPkUGVqwgYphHijXeUD6Ijv9uALYU7/TZAQJMxgLcAV77BPGDQbYzsuaQdsdtQDkxto4CduG1N4ZjC8wpta6bIUkR4nYJWIA49Z7w73XeHaA9Nu3E7X3BYX0zz8wX/TLl5r52AIoEoTrv8L6NaPBLix88cgmo853AIvvu38LQ7R0gzi7VpqyPVJ2YI6GuxKp++QMJJS+bAu1CbcQyv9yWGZCELXGMH6d1iUQ/Y+eu/9otGFS4LP78TbLEEqjO7wUpoisYACd8LsXej4sj1i7eJjg2rvihrBb66DghrmubBUkiGiSLx8cjzowNsQsSo00arOJSKEHEdqxfCNzbG6vqRzUL0uJTwKvAt0KfH60ljRAHwdZrscEEOCZ4sG1aMadNWZokTChJqyMVZQsyVmS59wv8EFdjFMCmEWDTMX0VJ0ZiioMyB4kl3vA0InPaZba6D/0vNMJ7QD5Y4uh3ulwCqUrPKdZx5RZZjJyYaQgTbJz6+gxvkgzTxDbD5iRxDRZAnIhyDVBumHJpdkps0+y97fVBt2+0NCjBVjo8BQAY0JX2E/UXAE3er0ROvn8fnMWKP4ty1cWqnyT21h+crLGNKwHPDkuhLQhdtn7H4PY2V6dfbRZP2ARybn3N26IdYztjd05x2vIU8EkA/sNP0i/uBn7c5DM5uwLMlYygxiLl+MguTwaCR1eMg5kw3BhDaYTzeghTD3YXjLwJzNZb5FB0mnQ9MBGJ6aWVe4e8ToPVlj8+uspufRUWlj+MAs494CyEqJhvDciAE9jdGOSoT7/qBhfAqhUNvso5TfHKUIrYQqC3LT/DNA9MischjXBsXBLpvUI5Om3GZHzbAQx89H3RMI1wG6UB2EItiCi7YLe+NJNFNkaEOZ02Y5TUc7SAyCvU6kiqX5iucjqCf4qVlyzcUMYeuabsLG0u7umjnBchb1pQ/tgoJ4Cyme7rX4DJY1PstZxqHDNBdOXqSujSkNrk0C7cAXOlAvhuin7VkzzDVWCEtdKks9y0jb5NyMpPvOAMPfGcw1qe/Xt6/jxmDiYF+b8CvhD5Suyu2Kupk8meVHv5IxsTRd0dNNhUp+ykkrIFqVKeomsL1y6J0S4nOOpdd/3b4HB9BMklfDzWqzCB8vqLIn+bswEk64P3sgPw+2oD8HUVY8f9siO0lWzCSU2GHPTdzTgKEi4shVlDx+o6Vqd8jtW1SWIl7M4Tjn3fA0zYukMAvP27+ikEOWTy4DrcOgWD1VDMqatN3Ulgd1LqO5bPcEQ3R4rfu0QD1KX/tPIajaFfqXJj5x6AMTVgzIuQYDsBaF/jLWBWcOvB7MKLqU4OPruoOkJnpTMhSuZsAefKZIuOyXQkm9NKDJfCTq4s6Uf82Jk+utoySF+S97OvwW/Uaj5ZfQB+DLXAfBTV8IjAtfDyDcN3605ZQMcmojxFKmZc/c18W3Xd0acJ+g02q1zZS0kZbOIxboNVb9JuDG+t8GDSAF9/0Vm8hsvq4bTJjDtwV+BjcS54ZG02Vpw2SZtc3czpODIcWJqaxJabqt2XyPj1LS+Fi1PnMaHjpJ/CxA1WzNCe00uAJuX5SM1ftOcGiM6nwH4RYqJ9QzyLRL+qhjNZSF3lqs6x9oNSoWPmTtLl+GTc4uTCFjTYuD3QW8eJq7z52NebICtlEXYrr8DIIUB7B6hwtvKzIGVtpw7fveLKkjsCWegm1JHRS9UnUQpLU1DWEBA+2SV1aAOMpTxkrPg5Hn2Mhz4NOO8B/v7PpUCwNMEoyBdSZT9OJXM80gUDPn2bnZYni/4lfBWbbFad2NGZ/AjFHKq1Y/QVNoYtqxubOimYco25BOw0E/pOm1um2doFxybC8Yl0PpX3Yya5NES4ReujjXGyGz+ZyQ3bdlRLVpACHF/z1s2j8kCCc9UDiGeK7w5Yjuj3q7D1Ak7tngwES43oTV57xsgmG7knJ6o8/DI+7cpQlhieJPHpjdHAZzMCNp/BIu66uqJRBDjNKIZYtemnvJslPjvAgiDvgIj7a5BF7xufRWGX7ZVUxk+2CZdLc+7HAJ1LqpAE1RsfvG9v2hmudwYHUMY5rXGud/VjDDt2gnrnSNEaId0tYofJbwEtTswugH3kZEWO5SVgFXcTVrylxjdFbNAS4iRudpnj4pw6YzmYHNqHkqh6zYcy4xhHqQwZK/yUB7xvkF4ac+THkL9UcvMDmAa0cIJcGmBiHBeaiS46G6EC20V/jFPo5N4GZLWMJ+G4G8Lw02UBTjpxu0Hju7o2GGJ4/VP3ekqYnjj3mpeBwaj7tgMoIpcAenC3l5qVW1Q5MU6xN81Nqg1uI+2uqi0bm7Fzq8+yi3MMP69Vg4oJdvDBqRNbnaCh+qdJ6MRk5cVSc2BvfP7yv0OyAzB457cTTujNwFndFTokew0lbDZpDxWI0ee9wzRHjFhpYoWbgcloUxyehEdfHcPb9hbnmFjRM04c57vDaZKCc6HBKBs3LDdBCrUzsOwA4oZSvEmX0BqswwvXAJ0NuRRdsJggh4++iV94AlKUq8XcSdjc8UkIZQ9tKpRPGixs+dgADzB9HOY/RLjp5itqPoJYOhNkWZWhVIxT5HKCtqZwcdBF18G9QRyHiQgxsMlnVR2b8D2pP20tajgsPi7A3vrF1I8x9Yc6x1OfAlazdwByG4C4iIQuDZjkaw4HIDf3i/6RcpLThJhmngV2l2wduCQtr+xEpVM3sWva3Ikgx70U0gDvBvmF5SVAIAtNJcocvV63rA4f2Scy+iclCtWEjU15cGGeJO3QLj6DuGZ7G1ZVG2ExF92JrzwFO9yPQR3ZB8Z52k8BJt+XwK3+JzNyEpNPERms08NCa7cYJ3hA5tKmttiMuRTluTTwNX4O8TpJ6iTtJXXFz5wdJ9bg1t0PAKTK00dehQngLjgvAVRrK0T4xslIj8yqbcLXbCaBrfY2K2NjlsQUO4kbMr6HPrp7H8bVn8VbeA//DSDvnb4M+SqsQx5/ClAvhTV6fG7yj61La27049IQdbnxqVeGGisFqhuDTAoenAV0V2xMG6LNWBNPP0nfXO5w/UPnDnALuGo+b2TfU3wnlzvpI3pkWBmuHPTJKhuDadU2XhBjDy6KNVdEa9FeY8fFGQ9d8tTGIY15/jDCyB0QoIVLBoTkgtdg7gM1n/oTF/DdySCQBaY4eMYDi+wJ6oRrNMWKU0HyxugOiFqTeu3KA43Nm92QdovX93YT9KmIwr+OanD7I6b+cHQJ5OlbNLhvQWJb86+gA8Z1F22SqqMbriyd+gBGmYKJm90jP/wizzj3gcNmve/5HYDgDsgHkJkoxdsIx6Mz2P8kfIAn2a9iBUDaw0+fi2FF2KppXpz0sdgxbgzCjjvxt01/dXCLj957oO/A2QEYvf7tpFsgLCgG/w9NjGZycTU0MdXlJD/H5hbjUQg+G2PssYuLXvvowwdfn8AH23cBbfrnH43yc5ixj0Fv1TpIiJ+SXZbvOBu/xRBX2WzzhDHBFmKolcu6dtdw9WB8srVP3IlXngN28cvgLl6uf4Gl/F2AidwB6pzMuWC7EUm89gOTiU+cjicN1i+vW80tp3ITbJw0RR/IE+J6KkyzMlZuLsUMHrZ8yo9YMeBrjMQezPoW4CWgYYpJ5a7aSfp1cvitOyfo27JTbIRFdHAaBhObskmLG0zYyOoO04IY1zy13TcKnR3Q5lMA2MuHfxp75VN4LgOi4ZNtf74MOUnIwCWDV/5e7uSlkbfCcRMuH2yrvBRlLgcO9xXqjIP/9lUeTLjFI3z8F+ualIvlptDJAAAAAElFTkSuQmCC";
class WallTrailMaterialProperty extends MaterialProperty {
  constructor(options = {}) {
    super(options);
    this._image = void 0;
    this._imageSubscription = void 0;
    this.image = IMG;
  }
  getType(time) {
    return Cesium.Material.WallTrailType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.color = Cesium.Property.getValueOrUndefined(this._color, time);
    result.image = Cesium.Property.getValueOrUndefined(this._image, time);
    result.speed = this._speed;
    return result;
  }
  equals(other) {
    return this === other || other instanceof WallTrailMaterialProperty && Cesium.Property.equals(this._color, other._color) && Cesium.Property.equals(this._speed, other._speed);
  }
}
Object.defineProperties(WallTrailMaterialProperty.prototype, {
  color: Cesium.createPropertyDescriptor("color"),
  speed: Cesium.createPropertyDescriptor("speed"),
  image: Cesium.createPropertyDescriptor("image")
});
class WaterMaterialProperty {
  constructor(options) {
    options = options || {};
    this._definitionChanged = new Cesium.Event();
    this._baseWaterColor = void 0;
    this._baseWaterColorSubscription = void 0;
    this.baseWaterColor = options.baseWaterColor || new Cesium.Color(0.2, 0.3, 0.6, 1);
    this._blendColor = void 0;
    this._blendColorSubscription = void 0;
    this.blendColor = options.blendColor || new Cesium.Color(0, 1, 0.699, 1);
    this._specularMap = void 0;
    this._specularMapSubscription = void 0;
    this.specularMap = options.specularMap || Cesium.Material.DefaultImageId;
    this._normalMap = void 0;
    this._normalMapSubscription = void 0;
    this.normalMap = options.normalMap || Cesium.Material.DefaultImageId;
    this.frequency = Cesium.defaultValue(options.frequency, 1e3);
    this.animationSpeed = Cesium.defaultValue(options.animationSpeed, 0.01);
    this.amplitude = Cesium.defaultValue(options.amplitude, 10);
    this.specularIntensity = Cesium.defaultValue(options.specularIntensity, 0.5);
  }
  get isConstant() {
    return false;
  }
  get definitionChanged() {
    return this._definitionChanged;
  }
  getType(time) {
    return Cesium.Material.WaterType;
  }
  getValue(time, result) {
    if (!result) {
      result = {};
    }
    result.baseWaterColor = Cesium.Property.getValueOrUndefined(this._baseWaterColor, time);
    result.blendColor = Cesium.Property.getValueOrUndefined(this._blendColor, time);
    result.specularMap = Cesium.Property.getValueOrUndefined(this._specularMap, time);
    result.normalMap = Cesium.Property.getValueOrUndefined(this._normalMap, time);
    result.frequency = this.frequency;
    result.animationSpeed = this.animationSpeed;
    result.amplitude = this.amplitude;
    result.specularIntensity = this.specularIntensity;
    return result;
  }
  equals(other) {
    return this === other || other instanceof WaterMaterialProperty && Cesium.Property.equals(this._baseWaterColor, other._baseWaterColor);
  }
}
Object.defineProperties(WaterMaterialProperty.prototype, {
  baseWaterColor: Cesium.createPropertyDescriptor("baseWaterColor"),
  blendColor: Cesium.createPropertyDescriptor("blendColor"),
  specularMap: Cesium.createPropertyDescriptor("specularMap"),
  normalMap: Cesium.createPropertyDescriptor("normalMap")
});
export { CircleBlurMaterialProperty, CircleDiffuseMaterialProperty, CircleFadeMaterialProperty, CirclePulseMaterialProperty, CircleScanMaterialProperty, CircleSpiralMaterialProperty, CircleVaryMaterialProperty, CircleWaveMaterialProperty, EllipsoidElectricMaterialProperty, EllipsoidTrailMaterialProperty, PolylineFlickerMaterialProperty, PolylineFlowMaterialProperty, PolylineImageTrailMaterialProperty, PolylineLightingMaterialProperty, PolylineLightingTrailMaterialProperty, PolylineTrailMaterialProperty, RadarLineMaterialProperty, RadarSweepMaterialProperty, RadarWaveMaterialProperty, WallImageTrailMaterialProperty, WallLineTrailMaterialProperty, WallTrailMaterialProperty, WaterMaterialProperty };
