import { defineConfig } from "vite";
import glsl from 'vite-plugin-glsl';
import cesium from "vite-plugin-cesium";
import { resolve } from 'path'
const lib_name = 'cesium-material';
export default defineConfig({
  plugins: [glsl()],
  alias: {
    '@': resolve(__dirname, 'src')
  },
  build: {
    lib: {
      entry: resolve(__dirname, 'src/material/index.js'),
      name: lib_name,
      fileName: (format) => `${lib_name}.${format}.js`
    },
    minify: "terser",
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['cesium'],
      output: {
        assetFileNames: `${lib_name}.css`,
        globals: {
          cesium: 'Cesium'
        }
      }
    },
    terserOptions: {
      compress: {
        drop_console: true
      }
    }
  }
});